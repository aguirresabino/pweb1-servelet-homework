/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.model.manager;

import br.com.ifpb.model.dao.ClienteDaoIF;
import br.com.ifpb.model.factory.DaoFactory;
import br.com.ifpb.model.factory.DaoFactoryIF;
import br.com.ifpb.model.valueObject.Cliente;
import java.util.List;

/**
 *
 * @author aguirre
 */
public class ClienteManager implements ClienteDaoIF{
    private DaoFactoryIF factory = null;
    private ClienteDaoIF clienteDao = null;
    
    public ClienteManager(){
        factory = DaoFactory.createFactory();
        clienteDao = factory.criaClienteDao();
    }

    @Override
    public Cliente getClienteById(int id) {
        return clienteDao.getClienteById(id);
    }

    @Override
    public boolean incluir(Cliente o) {
        return clienteDao.incluir(o);
    }

    @Override
    public boolean alterar(Cliente o) {
        return clienteDao.alterar(o);
    }

    @Override
    public boolean excluir(Cliente o) {
        return clienteDao.excluir(o);
    }

    @Override
    public List<Cliente> listar() {
        return clienteDao.listar();
    }

    @Override
    public Cliente getClienteByDocumento(String documento) {
        return clienteDao.getClienteByDocumento(documento);
    }
    
    
}
