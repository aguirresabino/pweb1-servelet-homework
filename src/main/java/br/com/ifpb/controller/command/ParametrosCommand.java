/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.controller.command;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aguirre
 */
public class ParametrosCommand implements CommandIF{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {
        PrintWriter out = response.getWriter();
        out.println("<html><head><title> Atividade </title></head>");
        out.println("<body><h3>Parâmetros</h3>");
        out.println("<p><b>Parâmetro 1: </b>"+ request.getParameter("param1") + "</p>");
        out.println("<p><b>Parâmetro 2: </b>"+ request.getParameter("param2") + "</p>");
        out.println("<p><b>Parâmetro 3: </b>"+ request.getParameter("param3") + "</p>");
        out.println("</body>");
        out.println("</html>");
        
    }
    
}
