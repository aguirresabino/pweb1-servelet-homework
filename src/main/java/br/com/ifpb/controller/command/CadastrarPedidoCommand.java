/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.controller.command;

import br.com.ifpb.model.dao.ClienteDaoIF;
import br.com.ifpb.model.dao.PedidoDaoIF;
import br.com.ifpb.model.factory.DaoFactory;
import br.com.ifpb.model.valueObject.Cliente;
import br.com.ifpb.model.valueObject.Pedido;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aguirre
 */
public class CadastrarPedidoCommand implements CommandIF{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {
        ClienteDaoIF clienteDao = DaoFactory.createFactory().criaClienteDao();
        PedidoDaoIF pedidoDao = DaoFactory.createFactory().criaPedidoDao();
        
        Cliente cliente = clienteDao.getClienteByDocumento(request.getParameter("documento"));
        Pedido pedido = new Pedido();
        
        pedido.setCliente(cliente);
        pedido.setValor(Double.parseDouble(request.getParameter("valor")));
        
        System.out.println("aqui");
        
         if(pedidoDao.incluir(pedido)){
             System.out.println("aqui");
            request.getRequestDispatcher("alert.html").include(request, response);
        }else{
            PrintWriter out = response.getWriter();
            out.println("Error");
        }
    }
}