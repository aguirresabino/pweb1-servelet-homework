/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.controller.command;

import br.com.ifpb.model.dao.ClienteDaoIF;
import br.com.ifpb.model.factory.DaoFactory;
import br.com.ifpb.model.valueObject.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aguirre
 */
public class ConsultarClienteCommand implements CommandIF{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {
        ClienteDaoIF clienteDao = DaoFactory.createFactory().criaClienteDao();
        
        Cliente cliente = clienteDao.getClienteByDocumento(request.getParameter("busca"));
        request.setAttribute("nome", cliente.getNome());
        request.setAttribute("documento", cliente.getDocumento());
        request.setAttribute("saldo", cliente.getSaldo());
        request.setAttribute("ativo", cliente.getAtivo());
        request.getRequestDispatcher("index.jsp").include(request, response);
    }
    
}
