/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.controller.command;

import br.com.ifpb.model.dao.ClienteDaoIF;
import br.com.ifpb.model.factory.DaoFactory;
import br.com.ifpb.model.valueObject.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aguirre
 */
public class CadastroClienteCommand implements CommandIF{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {

        ClienteDaoIF clienteDao = DaoFactory.createFactory().criaClienteDao();
        Cliente cliente = new Cliente();
        
        cliente.setNome(request.getParameter("nome"));
        cliente.setDocumento(request.getParameter("documento"));
        cliente.setSaldo(Double.parseDouble(request.getParameter("saldo")));
        
        if(clienteDao.incluir(cliente)){
            request.getRequestDispatcher("alert.html").include(request, response);
        }else{
            PrintWriter out = response.getWriter();
            out.println("Error");
        }
    }
    
}
