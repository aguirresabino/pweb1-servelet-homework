/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.controller.command;

import br.com.ifpb.model.dao.ClienteDaoIF;
import br.com.ifpb.model.dao.PedidoDaoIF;
import br.com.ifpb.model.factory.DaoFactory;
import br.com.ifpb.model.valueObject.Cliente;
import br.com.ifpb.model.valueObject.Pedido;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aguirre
 */
public class ConsultarPedidoCommand implements CommandIF {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {
        ClienteDaoIF clienteDao = DaoFactory.createFactory().criaClienteDao();
        PedidoDaoIF pedidoDao = DaoFactory.createFactory().criaPedidoDao();
        
        Cliente cliente = clienteDao.getClienteByDocumento(request.getParameter("busca"));
        List<Pedido> pedidos = pedidoDao.getPedidoByCliente(cliente);
        
        PrintWriter out = response.getWriter();
        out.println("<html><head><title> Atividade </title></head>");
        out.println("<body><h3>Pedidos</h3>");
        for(Pedido pedido : pedidos){
            out.println("<p>ID: " + pedido.getId() + " Valor: " + pedido.getValor() + " Data: " + pedido.getData() + "</p><br>");
        }
        out.println("</body>");
        out.println("</html>");
    }
    
}
