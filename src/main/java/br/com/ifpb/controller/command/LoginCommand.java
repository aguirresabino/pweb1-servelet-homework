/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.controller.command;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author aguirre
 */
public class LoginCommand implements CommandIF{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {
        if (request.getParameter("senha").equals("admin123")) {
            HttpSession session = request.getSession();
            session.setAttribute("nome", request.getParameter("nome"));
            session.setAttribute("senha", request.getParameter("senha"));
            PrintWriter out = response.getWriter();
            out.println("Usuário logado");
        }
    }
    
}
