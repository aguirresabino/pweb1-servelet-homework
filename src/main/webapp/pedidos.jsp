<%-- 
    Document   : pedidos
    Created on : 06/03/2018, 01:26:56
    Author     : aguirre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Atividade</title>
    </head>
    <body>
        <h3>Cadastrar pedidos</h3>
        <form action="front" method="POST">
            Documento cliente: <input type="text" name="documento">
            Valor: <input type="text" name="valor">
            <input type="submit">
            <input type="hidden" name="command" value="CadastrarPedido">
        </form>
        <h4>Consultar pedidos cadastrados por usuário</h4>
        <form action="front" method="POST">
            Documento do cliente: <input type="text" name="busca">
            <input type="submit">
            <input type="hidden" name="command" value="ConsultarPedido">
        </form>
    </body>
</html>
