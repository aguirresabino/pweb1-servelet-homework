<%-- 
    Document   : index
    Created on : 05/03/2018, 21:39:34
    Author     : aguirre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Atividade</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h3>Cadastrar Usuário</h3>
        <form action="front" method="POST">
            Nome: <input type="text" name="nome">
            Documento: <input type="text" name="documento">
            Saldo: <input type="text" name="saldo">
            <input type="submit">
            <input type="hidden" name="command" value="CadastroCliente">
        </form>
        <hr>
        <h3>Buscar dados de usuários cadastrados</h3>
        <form action="front" method="GET">
            Buscar   : <input type="text" name="busca"> <input type="submit"><br>
            Nome     : <input disabled type="text" value="<%= request.getAttribute("nome")%>">
            Documento: <input disabled type="text" value='<%= request.getAttribute("documento") %>'>
            Saldo    : <input disabled type="text" value='<%= request.getAttribute("saldo") %>'>
            Ativo    : <input disabled type="text" value='<%= request.getAttribute("ativo") %>'>
            <input type="hidden" name="command" value="ConsultarCliente">
        </form>
            <hr>
            <h4><a href="pedidos.jsp">Ir para tela de pedidos</a></h4><br>
            <h4><a href="parametroInicial">Questão 2</a></h4>
            <h4><a href="sessao">Questão 5</a></h4>
            <h4><a href="login.html">Questão 6</a></h4>
    </body>
</html>

